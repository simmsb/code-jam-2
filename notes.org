#+TITLE: Notes for api

* Endpoints
** Card inspection
- View cards in deck
- Vew specs for card
** Game control
- Get player (GET), returns a token for the user, used for opening the
  websocket, playing attacks, getting game stats etc
- Game Connect (WS), Creates a ws connection to the api, enters the user into the
  game queue.
- Get game state (GET) -- state of game, same for both players
- Get card state (GET) -- state of cards, unique for each player
- Leave game (DELETE)
** Game play
- Play move (POST)
* Gameplay
- Each card has a list of abilities, and the card type
- Card types are one of (saber, archer, lancer)
- Ability types are one of (ranged, melee, magic)
- Game consists of using attacks on opponents cards
- RPS style game, saber does 1.2x damage to archer, no damage reduction though
  (saber -> archer, archer -> lancer, lancer -> saber)
- Certain ability types do 0.8x damage to certain card types
  (ranged -> archer, melee -> saber, magic -> lancer)
- Cards have a starting hp of 200, when they reach 0hp they will no longer be
  able to attack.
- Each player receives 10 cards at the start of the game, the loser is the first
  to have all their cards drop to 0hp
* Database
- rethinkdb but pretend it's relational anyway
** Table: players
Each time someone uses the enter_game endpoint a new player is created
The token is used for that player to spawn a game websocket
*** Columns
id - uuid
token - string
game_id - id
socket_connected - bool
** Table: games
Stores ongoing and finished games
*** Columns
id - uuid
player1_id - playerid
player2_id - playerid
start_time - unix timestamp
status - GameStatus enum
gamestate - json or something idk
** Table: cards
Stores cards that exist in the system
*** Columns
id - uuid
name - string
type - CardType enum
abilities - list of ability ids
health - integer
image - string url
** Table: game events
Stores each game event
*** Columns
id - uuid
type - EventType enum
game_id - gameid
data - json or something
** Table: abilities
Each ability that is usable
*** Columns
id - uuid
name - string
description - string
type - AbilityType enum
damage - int

* Websocket
- Broadcasts game events: (start, end, update) etc
- Ability uses, card given to player
- Need some reaper to kill games if one player fails to connect after a period
  or player leaves mid-game
- Only one ws connection allowed at once, if ws dies player should be able to
  reconnect (withing a suitable time delay before the game is reaped)
* Game flow
1. Client requests a player
2. Client requests to join a game, a game id is sent back
3. Client connects to websocket with game id and player id
4. Client requests game start, echoed back over ws

5. When game starts, both players notified over ws
6. Game events happen
7. Game ends, disconnect events sent over ws
8. Player marked as used, game marked as completed

Kinda like this?

#+BEGIN_SRC plantuml :file docs/images/gameflow.png
actor Client as C
participant "/players/new" as GP
participant "/games/join_game" as JG
participant "/games/attack/<game_id>" as GA
control WebSocket as WS
database "Database" as DB

C->GP: Requesting a player
GP->DB: Add player info
GP->C: User token

C->JG: Joining game
JG->C: Game id

C->WS: Making WS Connection
WS->DB: Update player, queue game
activate WS
DB->WS: Game ready to use
WS->C: Game ready
C->WS: Enter Game
WS->DB: Game initialised
WS->C: Game information
C->GA: Play ability of card
GA->DB: Game update
DB->WS: Game update notify
WS->C: Acknowlegement of game event
DB->WS: Game finished
WS->C: Game ended
destroy WS
#+END_SRC

#+RESULTS:
[[file:docs/images/gameflow.png]]

* Env Vars
TODO move this section to documentation
- =RDB_HOST= :: RethinkDB hostname
- =RDB_PORT= :: RethinkDB port
- =RDB_NAME= :: Name of our database
