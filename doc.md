<a name="top"></a>
# code-jam-2-sour-uncles v0.0.0



- [Abilities](#abilities)
	- [](#)
	- [](#)
	
- [Cards](#cards)
	- [](#)
	- [](#)
	
- [Errors](#errors)
	- [](#)
	- [](#)
	- [](#)
	
- [Games](#games)
	- [Attack a card in a game.](#attack-a-card-in-a-game.)
	- [Join or start a game](#join-or-start-a-game)
	
- [Players](#players)
	- [Generate a player to use in playing a game](#generate-a-player-to-use-in-playing-a-game)
	
- [Ws](#ws)
	- [Connect to the game websocket.](#connect-to-the-game-websocket.)
	


# <a name='abilities'></a> Abilities

## <a name=''></a> 
[Back to top](#top)



	GET /abilities/:id





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | String | <p>ID of ability to get.</p>|

### Success Response

Success

```
HTTP/1.1 200 OK
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
    "name": "Agni Gandiva",
    "type": "magic",
    "damage": 20
}
```


## <a name=''></a> 
[Back to top](#top)



	GET /abilities/name/:name





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  name | String | <p>Name of ability to get.</p>|

### Success Response

Success

```
HTTP/1.1 200 OK
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
    "name": "Agni Gandiva",
    "type": "magic",
    "damage": 20
}
```


# <a name='cards'></a> Cards

## <a name=''></a> 
[Back to top](#top)



	GET /cards/:id





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  id | String | <p>ID of card to get.</p>|

### Success Response

Success

```
HTTP/1.1 200 OK
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
    "name": "Zeus",
    "type": "saber",
    "abilities": [
        "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
        "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7
    ]
}
```


## <a name=''></a> 
[Back to top](#top)



	GET /cards/name/:name





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  name | String | <p>Name of card to get.</p>|

### Success Response

Success

```
HTTP/1.1 200 OK
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
    "name": "Zeus",
    "type": "saber",
    "abilities": [
        "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
        "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7
    ]
}
```


# <a name='errors'></a> Errors

## <a name=''></a> 
[Back to top](#top)



	ERROR /






### Error Response

DataNotFound

```
HTTP/1.1 404 Not Found
{
    "message": "No user for id: '2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee' found",
    "key": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"
}
```
## <a name=''></a> 
[Back to top](#top)



	ERROR /






### Error Response

InvalidUsage

```
HTTP/1.1 400 Bad Request
{
    "message": "Incorrect token",
    "error_code": "token_incorrect"
}
```
## <a name=''></a> 
[Back to top](#top)

<p>The codes for the errors that can be in the error_code field of the InvalidUsage error.</p>

	ERROR /






# <a name='games'></a> Games

## <a name='attack-a-card-in-a-game.'></a> Attack a card in a game.
[Back to top](#top)



	POST /games/attack/:game_id





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  game_id | String | <p>ID of game to play attack in.</p>|

### Request Body Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  attacking_card_id | String | <p>ID of card to attack with</p>|
|  attacking_ability_id | String | <p>ID of ability to use</p>|
|  attacked_card_id | String | <p>ID of card to attack</p>|
|  player_id | String | <p>ID of player to attack as</p>|
|  token | String | <p>Token of player to attack as</p>|

### Success Response

Success

```
{
    "attacking_player": "player_2",
    "attacking_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee",
    "attacking_ability_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"",
    "attacked_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"",
    "new_state": {
        "player1_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
        "player2_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
        "current_turn": "player_1"
    }
}
```


## <a name='join-or-start-a-game'></a> Join or start a game
[Back to top](#top)



	POST /games/join_game





### Request Body Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  player_id | String | <p>ID of player to join as.</p>|
|  token | String | <p>Token for player to join as.</p>|

### Success Response

Success

```
HTTP/1.1 200 OK
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
    "status": "ready",
    "gamestate": {
        "player1_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
        "player2_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
        "current_turn": "player_1"
    },
    "player1_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee1",
    "player2_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "start_time": 12345,
    "last_update": 12346
}
```


# <a name='players'></a> Players

## <a name='generate-a-player-to-use-in-playing-a-game'></a> Generate a player to use in playing a game
[Back to top](#top)



	GET /players/new




### Success Response

Success

```
HTTP/1.1 200 OK
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
    "token": "qADBiiggxUCiHI_6TeQtwA",
    "game_id": null,
    "socket_connected": false
}
```


# <a name='ws'></a> Ws

## <a name='connect-to-the-game-websocket.'></a> Connect to the game websocket.
[Back to top](#top)

<p>The websocket sends json endoded data. The responses that can be given by the websocket are below:</p>

	WS /ws/:game_id/:player_id





### Parameter Parameters

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  game_id | string | <p>ID of game to connect to.</p>|
|  player_id | string | <p>ID of player to play as.</p>|

### Success Response

GameReady-event

```
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "type": "game_ready",
    "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "data": null
}
```
GameStart-event

```
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "type": "game_start",
    "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "data": null
}
```
GameEnd-event

```
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "type": "game_end",
    "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "data": {
        "winner_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2"
    }
}
```
GameCancel event

```
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "type": "game_cancel",
    "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "data": null
}
```
Attack event

```
{
    "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "type": "attack",
    "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
    "data": {
        "attacking_player": "player_2",
        "attacking_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee",
        "attacking_ability_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"",
        "attacked_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"",
        "new_state": {
            "player1_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
            "player2_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
            "current_turn": "player_1"
        }
    }
}
```

### Success 200

| Name     | Type       | Description                           |
|:---------|:-----------|:--------------------------------------|
|  GameReady |  | <p>event Signals that a game is ready to play. It will start as soon as both players are connected.</p>|
|  GameStart |  | <p>event Signals that a game has started. The player whose turn it is to attack can now attack.</p>|
|  GameEnd |  | <p>event Signals that the game has ended normally. The data field contians the player-id of the winner.</p>|
|  GameCancel |  | <p>event Signals that the game was cancelled</p>|
|  Attack |  | <p>event Signals an attack was made, contains details of the attack and the new state of the game.</p>|

