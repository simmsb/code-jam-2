import logging
import sys

from proj import create_app
from proj.database import db_delete, db_setup, generate_resources

if __name__ == '__main__':
    server, app, _, _, _ = create_app()

    # this is alright
    if "--delete" in sys.argv:
        db_delete(app)
        print("Deleted database.")
        exit(0)

    if "--setup" in sys.argv:
        db_setup(app)

    if "--generate" in sys.argv:
        generate_resources(app)

    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

    server.serve_forever()
