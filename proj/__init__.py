from flask import Flask, jsonify
from flask_dotenv import DotEnv
from flask_sockets import Sockets
from gevent import pywsgi
from geventwebsocket.handler import WebSocketHandler

from proj.database import Database
from proj.exceptions import DataNotFound, InternalError, InvalidUsage
from proj.routes import routes, socket_routes


def handle_exception(error):
    resp = jsonify(error.to_dict())
    resp.status_code = error.status_code
    return resp


def create_app():
    """Generate the flask app.

    sets up the env, socktio and database.
    """
    env = DotEnv()
    sockets = Sockets()
    db = Database()

    app = Flask(__name__)

    env.init_app(app)
    sockets.init_app(app)
    db.init_app(app)

    app.db = db

    for route in routes:
        app.register_blueprint(route)

    for route in socket_routes:
        sockets.register_blueprint(route)

    app.register_error_handler(DataNotFound, handle_exception)
    app.register_error_handler(InvalidUsage, handle_exception)
    app.register_error_handler(InternalError, handle_exception)

    server = pywsgi.WSGIServer(("", 5000), app, handler_class=WebSocketHandler)

    return server, app, env, sockets, db
