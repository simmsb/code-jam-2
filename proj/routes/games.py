import time

from flask import Blueprint, current_app, jsonify, request

from proj.data_objects import (EventType, Game, GameOverEvent, GameStatus,
                               PlayerType)
from proj.exceptions import InvalidUsage
from proj.game import process_attack, process_game_over
from proj.utils import ensure_params

games = Blueprint("games", __name__, url_prefix="/games")


@games.route("/join_game", methods=["POST"])
def add_player_to_game():
    """Join or start a game.

    @api {post} /games/join_game Join or start a game
    @apiName JoinGame
    @apiGroup Games

    @apiParam (Request Body) {String} player_id ID of player to join as.
    @apiParam (Request Body) {String} token     Token for player to join as.

    @apiParamExample {json} Request-Example
        {
            "player_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
            "token": "qADBiiggxUCiHI_6TeQtwA"
        }

    @apiSuccessExample {json} Success
        HTTP/1.1 200 OK
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
            "status": "ready",
            "gamestate": {
                "player1_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
                "player2_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
                "current_turn": "player_1"
            },
            "player1_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee1",
            "player2_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "start_time": 12345,
            "last_update": 12346
        }
    """
    data = request.get_json() or {}

    player_id, token = ensure_params(data, "player_id", "token")
    if not current_app.db.confirm_player(player_id, token):
        raise InvalidUsage("Incorrect token", "token_incorrect")

    player, game = current_app.db.join_or_spawn_game(player_id)

    return jsonify(game.to_dict())


@games.route("attack/<uuid:game_id>", methods=["POST"])
def attack_card(game_id):
    """Attack a card in a game.

    @api {post} /games/attack/:game_id Attack a card in a game.
    @apiName AttackCard
    @apiGroup Games

    @apiParam {String} game_id ID of game to play attack in.

    @apiParam (Request Body) {String} attacking_card_id    ID of card to attack with
    @apiParam (Request Body) {String} attacking_ability_id ID of ability to use
    @apiParam (Request Body) {String} attacked_card_id     ID of card to attack
    @apiParam (Request Body) {String} player_id            ID of player to attack as
    @apiParam (Request Body) {String} token                Token of player to attack as

    @apiParamExample {json} Request-Example
        {
            "attacking_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee1",
            "attacking_ability_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee1",
            "attacked_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee1",
            "player_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee1",
            "token": "qADBiiggxUCiHI_6TeQtwA"
        }

    @apiSuccessExample {json} Success
        {
            "attacking_player": "player_2",
            "attacking_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee",
            "attacking_ability_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"",
            "attacked_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"",
            "new_state": {
                "player1_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
                "player2_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
                "current_turn": "player_1"
            }
        }
    """
    data = request.get_json() or {}

    attacking_card_id, attacking_ability_id, attacked_card_id, player_id, token = ensure_params(
        data, "attacking_card_id", "attacking_ability_id", "attacked_card_id", "player_id", "token"
    )

    if not current_app.db.confirm_player(player_id, token):
        raise InvalidUsage("Incorrect token", "token_incorrect")

    game = Game.fetch(str(game_id), current_app.db)

    # can't play a game that isn't active
    if game.status is not GameStatus.ongoing:
        raise InvalidUsage("Game is not running.", "game_not_running")

    current_player_id = (game.player1_id
                         if game.gamestate.current_turn is PlayerType.player_1
                         else game.player2_id)

    if current_player_id != player_id:
        raise InvalidUsage("Cannot attack now", "cannot_attack_now")

    game, attack = process_attack(game, attacked_card_id, attacking_ability_id, attacked_card_id)
    game = game.update(last_update=int(time.time()))

    game.update_on_db(current_app.db)
    current_app.db.publish_game_event(EventType.attack, game.id, attack.to_dict())

    game_over, winner = process_game_over(game)

    if game_over:
        event = GameOverEvent(winner)
        current_app.db.publish_game_event(EventType.game_end, game.id, event.to_dict())
        game = game.update(status=GameStatus.finished)
        game.update_on_db(current_app.db)

    return jsonify(attack.to_dict())
