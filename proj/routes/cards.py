from flask import Blueprint, current_app, jsonify

from proj.data_objects import Card

cards = Blueprint("cards", __name__, url_prefix="/cards")


@cards.route("/<uuid:id>")
def get_card_by_id(id):
    """Retrieve a card by id.

    @api {get} /cards/:id
    @apiName GetCardByID
    @apiGroup Cards

    @apiParam {String} id ID of card to get.

    @apiSuccessExample {json} Success
        HTTP/1.1 200 OK
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
            "name": "Zeus",
            "type": "saber",
            "abilities": [
                "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
                "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7
            ]
        }
    """
    card = Card.fetch(str(id), current_app.db)
    return jsonify(card.to_dict())


@cards.route("/name/<name>")
def get_card_by_name(name):
    """Retrieve a card by name.

    @api {get} /cards/name/:name
    @apiName GetCardByName
    @apiGroup Cards

    @apiParam {String} name Name of card to get.

    @apiSuccessExample {json} Success
        HTTP/1.1 200 OK
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
            "name": "Zeus",
            "type": "saber",
            "abilities": [
                "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7",
                "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee7
            ]
        }
    """
    card = current_app.db.get_card(card_name=name)
    return jsonify(card.to_dict())
