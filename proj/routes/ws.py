from flask import Blueprint, current_app

from proj.data_objects import (EventType, Game, GameCancelEvent, GameStatus,
                               Player)
from proj.exceptions import InvalidUsage
from proj.utils import send_json

ws = Blueprint("ws", __name__, url_prefix="/ws")


@ws.route("/<uuid:game_id>/<uuid:player_id>")
def game_socket(ws, game_id, player_id):
    """Connect to the game websocket.

    @api {ws} /ws/:game_id/:player_id Connect to the game websocket.
    @apiName ConnectWebsocket
    @apiGroup Ws

    @apiParam {string} game_id   ID of game to connect to.
    @apiParam {string} player_id ID of player to play as.

    @apiDescription The websocket sends json endoded data.
                    The responses that can be given by the websocket are below:

    @apiSuccessExample {json} GameReady-event
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "type": "game_ready",
            "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "data": null
        }
    @apiSuccess GameReady event
        Signals that a game is ready to play.
        It will start as soon as both players are connected.

    @apiSuccessExample {json} GameStart-event
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "type": "game_start",
            "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "data": null
        }
    @apiSuccess GameStart event
        Signals that a game has started.
        The player whose turn it is to attack can now attack.

    @apiSuccessExample {json} GameEnd-event
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "type": "game_end",
            "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "data": {
                "winner_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2"
            }
        }
    @apiSuccess GameEnd event
        Signals that the game has ended normally.
        The data field contians the player-id of the winner.

    @apiSuccessExample {json} GameCancel event
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "type": "game_cancel",
            "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "data": null
        }
    @apiSuccess GameCancel event
        Signals that the game was cancelled

    @apiSuccessExample {json} Attack event
        {
            "id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "type": "attack",
            "game_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee2",
            "data": {
                "attacking_player": "player_2",
                "attacking_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee",
                "attacking_ability_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"",
                "attacked_card_id": "2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee"",
                "new_state": {
                    "player1_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
                    "player2_cards": {"2e5c2c1a-e5ac-498a-bb77-7fdd7e3f6ee": 200},
                    "current_turn": "player_1"
                }
            }
        }
    @apiSuccess Attack event
        Signals an attack was made, contains details of the attack and the new state of the game.
    """
    game_id = str(game_id)
    player_id = str(player_id)

    # mark the player as connected
    player = Player.fetch(player_id, current_app.db)
    if player.socket_connected:
        raise InvalidUsage(f"Player id: '{player_id}' already connected to websocket.", "already_connected")

    # you can only open a ws connection when the game is waiting or ready
    game = Game.fetch(game_id, current_app.db)
    if game.status not in (GameStatus.waiting, GameStatus.ready):
        raise InvalidUsage(f"Game is not in valid state to play.", "not_valid_state")

    if player_id not in (game.player1_id, game.player2_id):
        raise InvalidUsage("Player is not part of this game", "not_valid_player")

    player = player.update(socket_connected=True)
    current_app.db.update_player(player)

    # possibly start the game now that the player is connected
    current_app.db.maybe_start_game(game_id)

    # stream each event to the player
    event_stream = iter(current_app.db.event_stream(game_id))
    while not ws.closed:
        event = next(event_stream)
        send_json(ws, event)

        # close the socket if we got a game close event
        if event.type in (EventType.game_end, EventType.game_cancel):
            break

    player = current_app.db.get_player(player_id)
    player = player.update(socket_connected=False)
    current_app.db.update_player(player)

    # if the player disconnected while the game is running, cancel the game
    game = Game.fetch(game_id, current_app.db)
    if game.status is GameStatus.ongoing:
        current_app.db.publish_game_event(EventType.game_cancel, game_id,
                                          GameCancelEvent("Player disconnected from websocket"))
        game = game.update(status=GameStatus.cancelled)
