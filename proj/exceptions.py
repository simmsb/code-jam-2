from typing import Optional


class DataNotFound(LookupError):
    """Used for when something was not found."""

    status_code = 404

    def __init__(self, message: str, key: str, status_code: Optional[int] = None):
        super().__init__(message, key, status_code)
        self.message = message
        self.key = key

        if status_code is not None:
            self.status_code = status_code

    def to_dict(self):
        return {"message": self.message, "key": self.key}


class InvalidUsage(Exception):
    """Used for when a user does something that is not allowed."""

    status_code = 400

    def __init__(self, description: str, error_code: str, status_code: Optional[int] = None):
        super().__init__(description, error_code, status_code)
        self.description = description
        self.error_code = error_code

        if status_code is not None:
            self.status_code = status_code

    def to_dict(self):
        return {"description": self.description, "error_code": self.error_code}


class InternalError(Exception):
    """Represents an internal application error."""

    status_code = 503

    def __init__(self, reason: str, status_code: Optional[int] = None):
        super().__init__(reason, status_code)
        self.reason = reason

        if status_code is not None:
            self.status_code = status_code

    def to_dict(self):
        return {"reason": self.reason}
