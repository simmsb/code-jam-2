from typing import Optional, Tuple

from flask import current_app

from proj.data_objects import (Ability, AbilityType, AttackEvent, Card,
                               CardType, Game, GameState, PlayerID, PlayerType)
from proj.exceptions import InvalidUsage
from proj.utils import ImmutableDict


def generate_game_state(initial_cards=10, base_hp=200) -> GameState:
    """Generate the initial state of a game."""
    player1_cards = current_app.db.get_random_cards(initial_cards)
    player2_cards = current_app.db.get_random_cards(initial_cards)
    return GameState(
        player1_cards=ImmutableDict({card.id: base_hp for card in player1_cards}),
        player2_cards=ImmutableDict({card.id: base_hp for card in player2_cards}),
    )


# map for 1.2x damage multiplication between card types
# first element is the attacking card type
# second element is the attacked card type
class_damage_multiply_map = (
    (CardType.saber, CardType.archer),
    (CardType.archer, CardType.lancer),
    (CardType.lancer, CardType.saber)
)


# map for 0.8x damage multiplication between ability types and card types
# first element is the attacking ability type
# second element is the attacked card type
ability_damage_reduction_map = (
    (AbilityType.ranged, CardType.archer),
    (AbilityType.melee, CardType.saber),
    (AbilityType.magic, CardType.lancer)
)


def process_attack(game: Game, attacking_card_id: str,
                   attacking_ability_id: str, attacked_card_id: str) -> Tuple[Game, AttackEvent]:
    """Process an attack.

    :returns: A tuple of the updated game and the attack event to publish.
    """
    state = game.gamestate

    attacking_cards, attacked_cards = ((state.player1_cards, state.player2_cards)
                                       if state.current_turn is PlayerType.player_1
                                       else (state.player2_cards, state.player1_cards))

    # make sure the attacking and attacked cards are valid
    if attacking_card_id not in attacking_cards:
        raise InvalidUsage(f"Attacking card id: {attacking_card_id} is not a valid card", "not_valid_card")

    if attacked_card_id not in attacked_cards:
        raise InvalidUsage("Attacked card id: {attacked_card_id} is not a valid card", "not_valid_card")

    attacking_card = Card.fetch(attacking_card_id, current_app.db)
    attacking_ability = Ability.fetch(attacking_ability_id, current_app.db)
    attacked_card = Card.fetch(attacked_card_id, current_app.db)

    # make sure that the ability being used is an ability of the attacking card
    if attacking_ability_id not in attacked_card.abilities:
        raise InvalidUsage(f"Ability id: {attacking_ability_id} is not an ability of card id: {attacking_card.id}",
                           "not_valid_ability")

    # make sure that both cards are above 0 hp
    if attacking_cards[attacking_card_id] <= 0:
        raise InvalidUsage(f"Attacking card id: {attacking_card_id} has zero hp and cannot be used!", "dead_card")

    if attacked_cards[attacked_card_id] <= 0:
        raise InvalidUsage(f"Attacked card id: {attacked_card_id} has zero hp and cannot be attacked!", "dead_card")

    # the base damage to be done
    damage = attacking_ability.damage

    # apply damage multipliers
    if (attacking_card.type, attacked_card.type) in class_damage_multiply_map:
        damage *= 1.2

    if (attacking_ability.type, attacked_card.type) in ability_damage_reduction_map:
        damage *= 0.8

    # work out the new state
    if state.current_turn is PlayerType.player_1:
        attacked_card_new_hp = state.player2_cards[attacked_card_id] - damage
        if attacked_card_new_hp < 0:
            attacked_card_new_hp = 0

        new_state = state.update(
            state.player2_cards.set(attacked_card_id, attacked_card_new_hp),
            current_turn=PlayerType.player_2
        )
    else:
        attacked_card_new_hp = state.player1_cards[attacked_card_id] - damage
        if attacked_card_new_hp < 0:
            attacked_card_new_hp = 0

        new_state = state.update(
            state.player1_cards.set(attacked_card_id, attacked_card_new_hp),
            current_turn=PlayerType.player_1
        )

    # update game, publish attack event
    new_game = game.update(gamestate=new_state)
    attack = AttackEvent(state.current_turn, attacking_card_id, attacking_ability_id, attacked_card_id, new_state)

    return new_game, attack


def process_game_over(game: Game) -> Tuple[bool, Optional[PlayerID]]:
    """Determines if a game is over.

    :returns: Tuple where the first element is if the game is completed.
              The second the winning player id, if the game was won.
    """
    if all(i <= 0 for i in game.gamestate.player1_cards.values()):
        return True, game.player2_id

    if all(i <= 0 for i in game.gamestate.player2_cards.values()):
        return True, game.player1_id

    return False, None
