from proj.utils import ImmutableDict
from proj.data_objects import Player, Game, GameStatus, GameState


def test_serialise_deserialise_player():
    player = Player(id="fakeplayerid", token="fakeplayertoken")
    serialised = player.to_dict()

    assert serialised == {'id': 'fakeplayerid', 'token': 'fakeplayertoken', 'game_id': None, 'socket_connected': False}

    unserialised = player.from_dict(serialised)

    assert player == unserialised


def test_serialise_deserialise_game_gamestate():
    gamestate = GameState(ImmutableDict(), ImmutableDict())
    game = Game(id="fakegameid", status=GameStatus.finished, gamestate=gamestate, player1_id="fakeplayerid", start_time=1111)
    serialised = game.to_dict()

    assert serialised == {'id': 'fakegameid', 'status': 'finished',
                          'gamestate': {"player1_cards": {},
                                        "player2_cards": {},
                                        "current_turn": "player_1"},
                          'player1_id': "fakeplayerid", 'player2_id': None,
                          'start_time': 1111, "last_update": None}

    unserialised = game.from_dict(serialised)

    assert game == unserialised


def test_update():
    gamestate = GameState(ImmutableDict(), ImmutableDict())
    game = Game(id="fakegameid", status=GameStatus.ongoing, gamestate=gamestate, player1_id="fakeplayerid", start_time=1111)

    new_game = game.update(status=GameStatus.finished)

    assert game.status is GameStatus.ongoing
    assert new_game.status is GameStatus.finished
