# Some mythology card attack game

# Gameplay

This is an api for a card attack game, it revolves around two players attacking
each others cards until the hp all of one player's cards drops to zero.

A game has two players, and each player has a set of cards.

Cards have abilities that they can attack with, if a card drops to zero hp that
card can no longer be used to attack.

An attack works by one player attacking with one of their cards using an ability
of that card, the attack targets one of the enemy players cards, dealing damage.

After a player has attacked, the event is echoed over a websocket to all
players, the other player can now attack.

# API Docs
[API documentation](doc.md)

The flow kinda looks like this:
![Game flow](docs/images/gameflow.png)

# Installing
1. Setup `.env` file containing:
  - `RDB_HOST` :: RethinkDB hostname
  - `RDB_PORT` :: RethinkDB port
  - `RDB_NAME` :: Name of our database
2. execute `run.py` with the arguments `--setup --generate` to setup the
   database tables and to generate cards and abilities.

# Generating docs
I used apidoc to generate docs, to generate docs run: `apidoc`
